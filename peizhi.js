//此处的规则供参考，其中多半其实都是默认值，可以根据个人习惯改写
module.exports = {
  singleQuote: true, //使用单引号
  jsxSingleQuote: true, // jsx中使用单引号
};
